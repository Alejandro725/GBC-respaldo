<?php
    //clase para renderizar componentes
    class Page
    {
        //metodo para el header
        public static function Header($title)
        {
            echo
            ("
                <head>
                    <meta charset='UTF-8'>
                    <title>$title</title>
                    <!--Import Google Icon Font-->
                    <link href='../css/fuentes.css' rel='stylesheet'>
                    <!--Import materialize.css-->
                    <link type='text/css' rel='stylesheet' href='../css/materialize.min.css'  media='screen,projection'/>
                    <link href='css/estilos.css' rel='stylesheet'> 
                    <link type='text/css' rel='stylesheet' href='../css/sweetalert2.min.css'  media='screen,projection'/>
                    <script type='text/javascript' src='../js/sweetalert2.min.js'></script>

                    <!--Let browser know website is optimized for mobile-->
                    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
                </head>
            ");
        }

        //Metodo para integrar los script
        public static function Scripts()
        {
            echo
            ("
                <!--Import jQuery before materialize.js-->
                
            ");
        }

        //Metodo para el footer
        public static function Footer()
        {
            $hoy = getdate();
            echo
            ("
                <footer class='page-footer grey darken-4'>
                    <div class='container'>
                        <div class='row'>
                            <div class='col s10 offset-s1 center-align'>
                                <h5 class='white-text'>Configuraciones</h5>
                                <p class='grey-text text-darekn-2'> 
                                    Sistema de administracion para el sitio web GBC
                                </p>
                                <p class='grey-text text-darekn-2'>
                                    $hoy[mday] $hoy[month] $hoy[year]
                                </p>
                            </div>

                            <div class='col s12 m4 center-align'>
                                <h4 class='white-text footer-title'>Sugerencias</h4>
                                <p class='grey-text text-darken-2 footer-content'>
                                    Un buen dia se empieza con la actitud, muestrale lo mejor de ti a tus clientes y ya veras como alegraras su dia. 
                                </p>
                            </div>

                            <div class='col s12 m4 center-align'>
                                <h5 class='white-text footer-title'>Contactos de soporte</h5>
                                <p class='grey-text text-darken-2 footer-content'><i class='material-icons white-text footer-icons'>phone</i>: 7268-7066</p>
                                <p class='grey-text text-darken-2 footer-content'><i class='material-icons white-text footer-icons'>face</i> Alejandro Melendez</p>
                                <p class='grey-text text-darken-2 footer-content'><i class='material-icons white-text footer-icons'>email</i> alejandro725@outlook.es</p>
                            </div>

                            <div class='col s12 m4 center-align'>
                                <h5 class='white-text footer-title'>Sitio Publico</h5>
                                <img class='footer-img' src='../img/logos/logo-fo.png'>
                            </div>
                        </div>
                    </div>
                    <div class='footer-copyright'>
                        <div class='container center-align'>
                            Copyright © 2017 Gourmet Burger Company. Todos los derechos reservados.
                        </div>
                    </div>
                </footer>   
            ");
        }
    }
?>