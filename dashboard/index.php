<!DOCTYPE html>
<html>

    <!--Se colocan los datos del header-->
    <?php
        require('../lib/Page.php');
        Page::Header('Bienvenido');
    ?>

    <body>
        <!--Se añade el navbar-->    
        <div class="navbar-fixed"> 
            <nav class='grey darken-4'>  
                <div class="nav-wrapper">
                    <a href="#" class="center brand-logo hide-on-large-only"><img class='navbar-img' src='../img/logos/GBC.png'></a>
                    <a href="#" class="left hide-on-med-and-down"><img class='navbar-img' src='../img/logos/GBC.png'></a>
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    <ul id="nav-mobile" class="left hide-on-med-and-down">
                        <li><a href="sass.html" class='white-text'>Inicio</a></li>
                        <li><a href="badges.html" class='white-text'>Menu GBC</a></li>
                        <li><a href="collapsible.html" class='white-text'>Promociones</a></li>
                        <li><a href="collapsible.html" class='white-text'>Imagenes del Slideshow</a></li>
                    </ul>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a href="sass.html" class='white-text'>Perfil</a></li>
                        <li><a href="badges.html" class='white-text'>Menu Cerrar Sesion</a></li>
                    </ul>
                </div>
            </nav>
        </div>  
        
        <ul class="side-nav" id="mobile-demo">
            <li><a href="sass.html" class='white-text'>Inicio</a></li>
            <li><a href="badges.html" class='white-text'>Menu GBC</a></li>
            <li><a href="collapsible.html" class='white-text'>Promociones</a></li>
            <li><a href="collapsible.html" class='white-text'>Imagenes del Slideshow</a></li>
            <li><a href="sass.html" class='white-text'>Perfil</a></li>
            <li><a href="badges.html" class='white-text'>Menu Cerrar Sesion</a></li>
        </ul>      

        

        <!--Se colocan el footer-->
        <?php
            Page::Footer();
        ?>
        <script type='text/javascript' src='../js/jquery.js'></script>
        <script type='text/javascript' src='../js/materialize.min.js'></script>
        <script type='text/javascript' src='/js/main.js'></script>
    </body>
<html>